---
title: "Nostr Communities"
date: 2023-09-02T08:34:14-05:00
draft: false
---

# Nostr communities

Recently the nostr nips has approved a moderated community standard into it's nips. [nip 72](https://github.com/nostr-protocol/nips/blob/master/72.md)

I've had time to read over the nip, try out it's various implementations and even contribute in the form of [creating a bot](https://gitlab.com/nostrwolf/communitybot) to help deal with some of it's downsides. (which is currently many but more to that later)

## So how does it work?

To understand how it works lets first take a little look at how nostr events are stored.

Nostr events are usually stored in a event table. Example columns might be, the pubkey of the creator, when the event was created, [the kind](https://github.com/nostr-protocol/nips/blob/master/01.md#kinds) Which is a number, tags which can be used for like sub queries (In postgres it's often a jsonb array). The content (Like the post) and maybe a few other fields/tables. 

When you create a new community it's given a name in the database which is

34550 +":" + public key of creator + ":" plus community name.

Along with that it saves a list of moderators. And that becomes the definition of the community. The newest such event or the one with the latest created at field becomes the community. 

so to update the moderator list you don't delete the entry, you just create a newer one. 

The reason being is deletes are not guaranteed in nostr. 

When a post is made it's made to the usual kind 1 with a tag saying it wants to be part of the community. 

A moderator will see that there is a new request to the community. When they "Approve" the post a new post is made of kind 4550. The content
of this post will be the serialized content of the request post. So this post will be signed with a moderators private key. And in the content
you'll find a post signed by the original person's id. Creating a chain of trust. 


## What it does well

This design makes nostr communities very decentralized. They can exist on multiple relays. if one goes down conversations can continue on another. 
If one relay bans it another can keep it going. It's done without any needed changes on the relay level. 100% accomplished on the client side.

Currently the best web client is https://satellite.earth/ and you can view communities in Amethyst. 

## downsides

Many down sides exist. 

1. All post need to be approved. Literally no one liked this idea but it was merged I guess because a better idea hasn't been implemented. 
2. Searching through communities can be cumbersome because there is multiple communities named the same. You don't know for sure if it's made by someone official. 
3. Naming is confusing. How it's labeled is confusing.  
4. Down votes can't really be trusted. It's nostr you can generate a million users a minute. 
5. Spam bots everywhere. I can't tell you enough how much I hate ai bots replying in every thread. Different clients honor block list different. Comments do not need to be approved to be seen (and approving all of them in a bot would be a waste of resources) so you can't ban someone from commenting in your community. Not really. I can do it on the relay level but most people use big relays. and if one relay allows the content it shows for everyone using that relay.


Also, satellite.earth is far from perfect. Searching communities is awful. It just loads them all up. You can't browse by relay. You can't change your relays. Some people are using it but it needs lots of work.

## Any way we can improve it?

For one i've made a bot so I can atleast post from other clients like Amethyst. And it will auto approve it. 

We could make mobile clients auto approve so we don't need always on bots that have to be hosted. Like a section that just says auto approve when you open up your browser and or app. 

We really need stickied post. This will be challenging because I think it will require a expire time or a delete event to be respected. 

The community browsing in Satellite can be improved.


## Over all Results?

Communities have a long way to go before they can replace reddit or lemmy and there might be easier ways to do that.

## A modest proposal.


Nostr is great at being a open protocol. But that also makes it highly suspectable to attacks, and spam. 
What I really want is decentralized identities like nostr but also the convenience of sites like reddit or lemmy. 

Lemmy's main problem is that when one instance defederates from another instance all users are cut off from all users. 
If one user still wants to see content from OffensiveInstance A, but hes signed up in Instance B and B defederates from A he can't. 

He has to maintain two identities. 

Another problem with the federation model is that because everything is federated you duplicate all the work on multiple instances. Mostly all of it. 

What if we went back to before lemmy was federated? When there were multiple instances. There was a app named Lemmur and it worked really well.

You could browse multiple instances and have multiple accounts. The only problem was that you HAD to have multiple accounts
and there was no way for others to verify if that account was you or not. 

But if we applied the same concepts of nostr to that time period of lemmy. We could add decentralized identities and not need federation to get the same effect. 

How?

Simple, you register by signing a post with your private key that contains your public key. That instance could have open sign ups, or it could approve it with email, registration applications, and or any other verification method currently used. 

Everything else would work the same. You would have a website where all the post would go. But clients would connect to multiple websites using the same public key. The same identity. You would get all the advantages of a traditional website but just use nostr's identity system. 


> But then post won't be duplicated, if a instance goes down those communities won't be accessible. 

This really isn't much different from how it works now. When a instance goes down activity dies in it's communities. Pictures go offline. We could still mirror community activity to nostr events to other relays so it's available. We could store it locally client side. lots of ways to fix this problem.

And by having a single source of truth for a community edits and such don't need to be duplicated everywhere. 

To me this fixes all the problems with defederation and makes it easier to understand. 

The main thing I want is to take the power of deplatforming from a admin and give that back to the users. While still having a quick website where things are easy to understand. 

I imagine nostr style decentralized identities will start to be applied to lots of designs in the future. 