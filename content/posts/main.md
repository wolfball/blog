---
title: "Why did wolfballs shutdown?"
date: 2023-07-24T12:32:18-05:00
draft: false
---

## Why did wolfballs shut down?

I think it's worth a blog post to describe why wolfballs shutdown and what plans are for the future. 
There is a link floating around of my words changed/altered so I want to put a official wolfballs statement that can stay up.

Wolfballs was set up during the death of ruqqus. I needed a place to vent my frustration with covid lockdowns. I loved how much engagement I got at ruqqus. Yes there was terrible stuff hosted there. But the left trolled the right, the right the left and overall it was a fun experience for the users. For the admins it was clearly to much stress. And I learned much about that over the year I hosted wolfballs.

When wolfballs was created there was nowhere on the internet you could say that the vaccines were not effective. You would instantly be banned. 

You could not question the results of the 2020 election and many people wanted to talk about it.

Gab was a thing but it was always crashing.

As the months rolled on twitter was bought by Elon and he allowed all of the conversations I wanted to have. There was some stuff I didn't agree with like Eliza blue getting her twerking pics deleted but overall it's been ok. Gab got better infrastructure and is became the defacto place to say anything within constitutional rights. 

Lemmy is based off of reddit's design and reddit is a shadow banning nightmare. It's users love censorship. All 10 real users. And the bots are everywhere there.

Which brings me another problem of lemmy. Bots. Bots control everything on lemmy instances. It is even more obvious now that lemmy.world grew so large.

Voting cannot work with modern day bots. Explaining why to the average user is difficult. But rest assured it is easy to throw up a thousand bots behind a rotating vpn that just upvotes anything you want.

Then there is defederation. It's absolutely nuts. A single admin can decide for all his thousands of users that someone else's opinion is not valid and no user who created an account on that instance can read it. 

This will always be a problem on lemmy because it's easier to join a instance than to host one and easier to defederate than to moderate and build moderation tools.

And so I found myself on wolfballs, censoring more than I wanted. I also found myself thinking negative things more than I wanted because I was surrounded by doom post from people banned from other places on the internet. 

There are horrible things about our society. I don't want to read about it every day.

I took a break to take a class and enjoy family life. Interestingly I was almost immediately offered several high paying jobs and ultimately had to turn them all down because I was given such a large promotion to stay where I was. 

## The future

![](https://i.imgur.com/bLls7KO.png)

So what do I recommend for others wanting to help voices be heard?

In short [Nostr](https://nostr.com/). Nostr solves the biggest problem with the fediverse. I will quote Alex Gleason on what that problem is.

> Nostr solves my most basic frustration with ActivityPub, which is that some of my followers on different servers can't see me because their admin said so. I'm tired of it.

[Alex Gleason](https://soapbox.pub/blog/mostr-fediverse-nostr-bridge/)

Lemmy and the fediverse has a power imbalance problem. It's digital oppression.

The admins have a unjust amount of power they apply towards their users. They openly federate and then with lots of resources they get lots of users. After thousands sign-up the defederation begins. Suddenly all the advantages of decentralization is gone because some turd sandwich doesn't like ONE other person's opinion.

There is value in moderation. Moderation is just another word for curation. But it must be optional. Users should be able to opt in and opt out. 

Nostr allows for that. Instead of 1 user pushing to one instance which then pushes to many other instances it's one user pushing to many instances and all users pull from many instances. 

It uses simple public private keys to verify user's identity.

Users can choose which instances (relays) they want to see content from. Admin's have no power to silence anyone because they do not control their private keys. They can only use public keys to verify that the message was sent by someone who has the private key associated with the public key.

With Nostr, it's as if every user has their own fediverse instance.

It's not free speech because a admin decides that his line of what is acceptable speech is more liberal than yours, it's freespeech because the technology does not allow to silence speech.

But people are free to only associate in circles they like and only pull from nodes that censor how they like.

## wrap up

Upon the realization of these things I closed down wolfballs, spent some time improving my Chinese and began using Nostr.

Kapow is doing a good job hosting [exploding-heads](https://exploding-heads.com) . Which I recommend users use. I have helped him out with UI code changes and even recently sent him some Etherium. (I suggest others do the same). It's very stressful offering a instance and only not free to the admin.

Over the next year I will be slowly (Because I just don't have the time) working on improving Nostr tools and will probably bring some of the concepts I found useful from lemmy to nostr. I will probably use other names and domains. Wolfballs the name was always a joke anyways. 